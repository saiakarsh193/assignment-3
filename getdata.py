def get_rocketcolor(player_number):
    path_1 = r"./resources/rocket_blue.png"
    path_2 = r"./resources/rocket_purple.png"
    f = open("config", "r")
    ln = f.readline()
    while(ln != ""):
        length = len(ln)
        end = 0
        for i in range(0, length):
            if(ln[i] == ':'):
                end = i
                break
        parameter = ln[0:end]
        if(parameter == 'Color of spaceship for Player 1'):
            color = ln[end+1:length-1]
            if(color == 'BLUE'):
                path_1 = r"./resources/rocket_blue.png"
            elif(color == 'RED'):
                path_1 = r"./resources/rocket_red.png"
            elif(color == 'PURPLE'):
                path_1 = r"./resources/rocket_purple.png"
            elif(color == 'GREEN'):
                path_1 = r"./resources/rocket_green.png"
            else:
                print("Wrong color option for Player 1!")
        elif(parameter == 'Color of spaceship for Player 2'):
            color = ln[end+1:length-1]
            if(color == 'BLUE'):
                path_2 = r"./resources/rocket_blue.png"
            elif(color == 'RED'):
                path_2 = r"./resources/rocket_red.png"
            elif(color == 'PURPLE'):
                path_2 = r"./resources/rocket_purple.png"
            elif(color == 'GREEN'):
                path_2 = r"./resources/rocket_green.png"
            else:
                print("Wrong color option for Player 2!")
        ln = f.readline()
    f.close()
    if(player_number == 1):
        return path_1
    else:
        return path_2


def get_rounds():
    rounds = 1
    f = open("config", "r")
    ln = f.readline()
    while(ln != ""):
        length = len(ln)
        end = 0
        for i in range(0, length):
            if(ln[i] == ':'):
                end = i
                break
        parameter = ln[0:end]
        if(parameter == 'Number of Rounds'):
            rounds = ln[end+1:length-1]
        ln = f.readline()
    f.close()
    rounds = int(rounds)
    if(rounds < 1 or rounds > 20):
        rounds = 1
        print("Number of rounds is not in given range! (Default is 1)")
    return rounds


def get_font():
    font = "freesansbold"
    f = open("config", "r")
    ln = f.readline()
    while(ln != ""):
        length = len(ln)
        end = 0
        for i in range(0, length):
            if(ln[i] == ':'):
                end = i
                break
        parameter = ln[0:end]
        if(parameter == 'Font Style'):
            font = ln[end+1:length-1]
        ln = f.readline()
    f.close()
    font += '.ttf'
    return font


def get_roundtime():
    rtime = 10.0
    f = open("config", "r")
    ln = f.readline()
    while(ln != ""):
        length = len(ln)
        end = 0
        for i in range(0, length):
            if(ln[i] == ':'):
                end = i
                break
        parameter = ln[0:end]
        if(parameter == 'Round Time'):
            rtime = ln[end+1:length-1]
        ln = f.readline()
    f.close()
    return float(rtime)
