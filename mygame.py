import pygame
import random
from getdata import *

# COLORS
white = (255, 255, 255)
black = (0, 0, 0)
blue = (4, 139, 207)
brown = (168, 95, 22)
grey = (79, 79, 79)

# PARAMETERS
no_of_rounds = get_rounds()
round_time = get_roundtime()

# SETUP
frame_rate = 20
x = 570
y = 880
blackhole_coordinates = []
rock_list = []
key_name = ""
time_left = round_time
player_1_score = 0
player_2_score = 0
current_player = 1
rock_speed = 0
game_over = True
path_crossed = [0] * 9

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((1200, 960))
pygame.display.set_caption("My space game!")
font = pygame.font.Font(get_font(), 24)
big_font = pygame.font.Font(get_font(), 60)

# IMAGE_VARIABLES
background = pygame.image.load(r'./resources/background.png')
background = pygame.transform.scale(background, (1200, 960))
blackhole = pygame.image.load(r'./resources/black_hole.png')
blackhole = pygame.transform.scale(blackhole, (80, 80))
player_1 = pygame.image.load(get_rocketcolor(1))
player_1 = pygame.transform.scale(player_1, (60, 80))
player_2 = pygame.image.load(get_rocketcolor(2))
player_2 = pygame.transform.scale(player_2, (60, 80))
player_2 = pygame.transform.rotate(player_2, 180)
rock_1 = pygame.image.load(r'./resources/rock_1.png')
rock_1 = pygame.transform.scale(rock_1, (100, 80))
rock_2 = pygame.image.load(r'./resources/rock_2.png')
rock_2 = pygame.transform.scale(rock_2, (100, 80))


def switch_player(condition):
    global current_player, time_left, x, y, player_1_score, player_2_score
    global no_of_rounds, rock_speed, game_over, path_crossed
    if(condition == 0):
        time_left = 0.0
    if(current_player == 1):
        player_1_score += int(time_left*10)
        current_player = 2
        x = 570
        y = 80
    else:
        no_of_rounds -= 1
        rock_speed += 5
        player_2_score += int(time_left*10)
        current_player = 1
        x = 570
        y = 880
    path_crossed = [0] * 9
    time_left = round_time
    if(no_of_rounds == 0):
        game_over = True


def move_player_coordinates():
    global x, y
    if(current_player == 1):
        if(key_name == 'up' and y != 80):
            y -= 80
        elif(key_name == 'down' and y != 880):
            y += 80
        elif(key_name == 'left' and x != 30):
            x -= 60
        elif(key_name == 'right' and x != 1110):
            x += 60
    elif(current_player == 2):
        if(key_name == 'w' and y != 80):
            y -= 80
        elif(key_name == 's' and y != 880):
            y += 80
        elif(key_name == 'a' and x != 30):
            x -= 60
        elif(key_name == 'd' and x != 1110):
            x += 60


def check_condition():
    global time_left, path_crossed, player_1_score, player_2_score
    time_left -= 0.05
    for i in blackhole_coordinates:
        if(x >= i[0]-50 and x <= i[0]+70 and y >= i[1]-70 and y <= i[1]+70):
            switch_player(0)
    for i in rock_list:
        if(x >= i[1]-50 and x <= i[1]+80 and y >= i[2]-70 and y <= i[2]+70):
            switch_player(0)
    if(current_player == 1):
        column = int(y/80)-1
    else:
        column = int(y/80)-3
    if(column >= 0 and column <= 8):
        if(path_crossed[column] != 1):
            path_crossed[column] = 1
            cross_score = 0
            if (column % 2 == 0):
                cross_score = 10
            else:
                cross_score = 5
            if(current_player == 1):
                player_1_score += cross_score
            else:
                player_2_score += cross_score
    if (y == 80 and current_player == 1):
        switch_player(1)
    elif(y == 880 and current_player == 2):
        switch_player(1)
    if (time_left <= 0):
        switch_player(0)


def move_rock_coordinates():
    global rock_list
    for i in rock_list:
        i[1] += rock_speed
        if(i[1] > 1200):
            i[1] = -100


def display_objects():
    screen.blit(background, (0, 0))
    pygame.draw.rect(screen, black, (0, 0, 1200, 80))
    screen.blit(font.render('Player 1:', True, white, black), (20, 10))
    p1s = str(player_1_score)
    screen.blit(font.render(p1s, True, white, black), (20, 40))
    screen.blit(font.render('Player 2:', True, white, black), (1000, 10))
    p2s = str(player_2_score)
    screen.blit(font.render(p2s, True, white, black), (1000, 40))
    if game_over is False:
        screen.blit(font.render('Time Left:', True, white, black), (700, 10))
        tl = str(round(time_left, 1))
        screen.blit(font.render(tl, True, white, black), (700, 40))
        txtobj = font.render('Number of round(s) left:', True, white, black)
        screen.blit(txtobj, (350, 10))
        rl = str(no_of_rounds)
        screen.blit(font.render(rl, True, white, black), (350, 40))
        if(current_player == 1):
            screen.blit(player_1, (x, y))
        else:
            screen.blit(player_2, (x, y))
        pygame.draw.rect(screen, grey, (0, 75, 1200, 5))
        for i in rock_list:
            if(i[0] == 1):
                screen.blit(rock_1, (i[1], i[2]))
            elif(i[0] == 2):
                screen.blit(rock_2, (i[1], i[2]))
        for i in blackhole_coordinates:
            screen.blit(blackhole, i)
    else:
        goobj = big_font.render('GAME OVER', True, white, black)
        screen.blit(goobj, (420, 300))
        if(player_1_score > player_2_score):
            p1wobj = big_font.render('Player 1 Wins!', True, white, black)
            screen.blit(p1wobj, (400, 400))
        elif(player_1_score < player_2_score):
            p2wobj = big_font.render('Player 2 Wins!', True, white, black)
            screen.blit(p2wobj, (400, 400))
        else:
            dgobj = big_font.render('Draw game!', True, white, black)
            screen.blit(dgobj, (420, 400))
        resobj = font.render('Press ENTER to restart', True, white, black)
        screen.blit(resobj, (470, 600))


def start_screen():
    screen.blit(background, (0, 0))
    txtobj = big_font.render('Space Jump!', True, white, black)
    screen.blit(txtobj, (400, 300))
    txtobj = font.render('Press ENTER to start', True, white, black)
    screen.blit(txtobj, (470, 800))
    txtobj = font.render('1. Player 1 controls: Arrrows', True, white, black)
    screen.blit(txtobj, (430, 450))
    txtobj = font.render('2. Player 2 controls: W S A D', True, white, black)
    screen.blit(txtobj, (430, 480))
    txtobj = font.render('3. Avoid obstacles', True, white, black)
    screen.blit(txtobj, (430, 510))
    tj = font.render('4. Cross an Asteroid: 10 points', True, white, black)
    screen.blit(tj, (430, 540))
    tj = font.render('5. Cross a Black hole: 5 points', True, white, black)
    screen.blit(tj, (430, 570))
    tj = font.render('6. You get time left x 10 points', True, white, black)
    screen.blit(tj, (430, 600))
    tj = font.render('7. You can change configurations', True, white, black)
    screen.blit(tj, (430, 630))


def start_game():
    global player_1_score, player_2_score
    global no_of_rounds, rock_speed, blackhole_coordinates, rock_list
    player_1_score = 0
    player_2_score = 0
    no_of_rounds = get_rounds()
    rock_speed = 10
    blackhole_coordinates = []
    rock_list = []
    for i in range(1, 5):
        obj_x1 = random.randrange(0, 520, 1)
        obj_x2 = random.randrange(600, 1120, 1)
        while(obj_x2-obj_x1 < 250):
            obj_x2 = random.randrange(600, 1120, 1)
        obj_y = 80+(160*i)
        blackhole_coordinates.append((obj_x1, obj_y))
        blackhole_coordinates.append((obj_x2, obj_y))
    for i in range(1, 6):
        obj_x1 = random.randrange(0, 500, 1)
        obj_x2 = random.randrange(500, 1100, 1)
        while(obj_x2-obj_x1 < 250):
            obj_x2 = random.randrange(500, 1100, 1)
        obj_y = 160*i
        rock_list.append([random.choice([1, 2]), obj_x1, obj_y])
        rock_list.append([random.choice([1, 2]), obj_x2, obj_y])


start_screen()
while True:
    clock.tick(frame_rate)
    pygame.display.update()
    for event in pygame.event.get():
        if(event.type == pygame.QUIT):
            pygame.quit()
            quit()
        if(event.type == pygame.KEYDOWN):
            key_name = pygame.key.name(event.key)
            if game_over is True:
                if(key_name == 'return'):
                    game_over = False
                    start_game()
            else:
                if key_name in ('up', 'down', 'left', 'right'):
                    move_player_coordinates()
                elif key_name in ('w', 's', 'a', 'd'):
                    move_player_coordinates()
    if game_over is True:
        continue
    check_condition()
    move_rock_coordinates()
    display_objects()
